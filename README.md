#RTS Base
RTS Base is going to be a basic RTS system built in HaxeFlixel.
A playable Demo can be found here: http://kingmaker.works/rtsb/RTS.swf

#Project Goals:
Must be simple to utilize
Must leverage tiled map editor


#Version .1 is finished! 
All the basics are in! Node based grid map built from tiled, with a* pathfinding, and Actors fighting utilizing statemachines! If you run it right now a demo level will play with some of my 8 bit art. All of which are free to use

#Verson .1.1 released
Basic Controls are implemented and a dash system is created. Multiple unit selection is a thing, and they all take the same orders. Movement is greatly improved.

#Version .2 Goals
Take in feedback from the community at large and contributors to projects that are going to utilize this system.
Adding buildings
Selecting and moving multiple units


.1 took a week of 1-2 hours a night, but I expect a month to get everything finished for v.2

I'm no longer making it a goal to be portable from HaxeFlixel. This is in part because I am enjoying HaxeFlixel, finding the performance of OpenFL as it grows more mature to be satisfactory, but also because I think that if i do port this it will be independant of calling this project a success.
